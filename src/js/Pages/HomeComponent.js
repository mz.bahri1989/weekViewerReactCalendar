import React from "react";
import WeeklyCalendar from "../sub/WeeklyCalendar";

class HomeComponent extends React.Component{
    constructor(props){
        super(props)
    }
    onChange(value){
        console.log(value)
    }
    render(){
        return <WeeklyCalendar onChange={this.onChange}/>
    }
}
export default HomeComponent