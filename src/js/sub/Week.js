import React from "react";

class Week extends React.Component {
    constructor(props) {
        super(props);
        this.setUpWeek = this.setUpWeek.bind(this);
        this.calculateDayToShow = this.calculateDayToShow.bind(this);
        this.clickHandler = this.clickHandler.bind(this);
    }

    componentDidMount() {

    }

    getMonthName(num) {
        const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        return month[num];
    }

    calculateDayToShow(day) {
        const dayString = new Date(day);
        return `${this.getMonthName(dayString.getMonth())} ${dayString.getDate()}`
    }

    setUpWeek() {
        const startDay = parseInt(this.props.startDay);

        let week = [null, null, null, null, null, null, null];
        const startDayIndex = new Date(startDay).getDay();
        week[startDayIndex] = {
            value: startDay,
            text: this.calculateDayToShow(startDay)
        };
        let i, j, count = 1;
        for (i = startDayIndex - 1; i > -1; i--) {
            const day = startDay - (count * 24 * 60 * 60 * 1000);
            week[i] = {
                value: day,
                text: this.calculateDayToShow(day)
            };
            count++;
        }
        count = 1;
        for (j = startDayIndex + 1; j < 7; j++) {
            const day = startDay + (count * 24 * 60 * 60 * 1000);
            week[j] = {
                value: day,
                text: this.calculateDayToShow(day)
            };
            count++;
        }
        return week;
    }

    clickHandler(e) {
        this.props.onChange(e.target.value)
    }

    render() {
        const week = this.setUpWeek();
        return <div className={`calendar__week__view ${this.props.className || ''}`}
        style={{minWidth:`${this.props.slideWidth}px`,
            maxWidth:`${this.props.slideWidth}px`}}>
            {
                week.map((day, index) => {
                        return <button
                            type="button"
                            className="calendar__week__item"
                            key={index}
                            onClick={this.clickHandler}
                            value={day.value}
                        >
                            {day.text}
                        </button>
                    }
                )
            }
        </div>
    }
}

export default Week;