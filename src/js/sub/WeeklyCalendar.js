import React from "react";
import Hammer from "hammerjs";
import Week from "./Week";
import "../../css/partials/__weekly.scss";

class WeeklyCalendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            weekList: [new Date().setHours(0, 0, 0, 0)],
            activeDay: new Date().setHours(0, 0, 0, 0),
            isPrev: false,
            isNext: false,
            translate: 0,
            width: 0
        };
        this.loadPreviousWeek = this.loadPreviousWeek.bind(this);
        this.loadNextWeek = this.loadNextWeek.bind(this);
        this.handleResizeOrRotation = this.handleResizeOrRotation.bind(this);
        this.document = document;
        this.wrapperWidth = 0;
        this.hammer = null;
    }

    componentDidMount() {
        const comp = this;
        const roller = document.querySelector('.calendar__week__roller');
        this.hammer = new Hammer(roller);
        this.hammer.on('swipeleft', () => {
            comp.loadNextWeek();
        });
        this.hammer.on('swiperight', () => {
            comp.loadPreviousWeek()
        });

        this.handleResizeOrRotation();
        window.addEventListener('resize', this.handleResizeOrRotation);
        window.addEventListener('orientationchange', this.handleResizeOrRotation)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResizeOrRotation);
        window.removeEventListener('orientationchange', this.handleResizeOrRotation);
        this.hammer.destroy();
        this.hammer = null;
    }

    handleResizeOrRotation() {
        this.wrapperWidth = this.document.querySelector('.calendar__week__wrapper').clientWidth;
        this.setState({
            translate: -1 * this.wrapperWidth,
            width: 3 * this.wrapperWidth,
            slideWidth: this.wrapperWidth
        })
    }

    loadPreviousWeek() {
        const weekList = this.state.weekList;
        const initialTranslate = parseInt(this.state.translate);
        const newActiveDay = this.state.activeDay - (7 * 24 * 60 * 60 * 1000);
        let width = parseInt(this.state.width);
        let translate = initialTranslate + this.wrapperWidth;

        if (weekList.indexOf(newActiveDay) === -1) {
            translate = initialTranslate;
            weekList.push(newActiveDay);
            weekList.sort((a, b) => a - b);
            width += this.wrapperWidth;
        }
        this.setState({
            weekList,
            activeDay: newActiveDay,
            isPrev: true,
            isNext: false,
            translate,
            width
        })
    }

    loadNextWeek() {
        const weekList = this.state.weekList;
        const newActiveDay = this.state.activeDay + (7 * 24 * 60 * 60 * 1000);
        const initialTranslate = parseInt(this.state.translate);
        const translate = initialTranslate - this.wrapperWidth;
        let width = parseInt(this.state.width);

        if (weekList.indexOf(newActiveDay) === -1) {
            weekList.push(newActiveDay);
            width += this.wrapperWidth;
        }
        this.setState({
            weekList,
            activeDay: newActiveDay,
            isNext: true,
            isPrev: false,
            translate,
            width
        })
    }

    render() {
        const style = {
            transform: `translateX(${this.state.translate}px)`,
            width: `${this.state.width}px`
        };
        return (
            <div className="calendar">
                <button type="button" className="calendar__week--toggler prev"
                        onClick={this.loadPreviousWeek}>prev
                </button>
                <div className="calendar__week__wrapper">
                    <div className="calendar__week__roller" style={style} draggable={true}>
                        {
                            this.state.weekList.map((week, index) => {
                                return <Week
                                    startDay={week} key={index}
                                    onChange={this.props.onChange}
                                    slideWidth={this.state.slideWidth}
                                />
                            })
                        }


                    </div>

                </div>
                <button type="button" className="calendar__week--toggler next"
                        onClick={this.loadNextWeek}>next
                </button>
            </div>
        )
    }
}

export default WeeklyCalendar